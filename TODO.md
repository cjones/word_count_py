Things that need to be improved
==========

- Could add test cases for the printing and argparse.
- Possibly need to touch up the documentation.
- Should the main class be initialized with a file name or no?
- Add in test for no file
- Add in test for wrong permissions
- Should switch to Python3 due to improvements and deprecation of Python2.7

Code review comments
==========

- [fixed] (d.a) Looks to be an errant `pass` line 20 of test_word_count_py.py
- [will not fix] (a.d/s.y.) The argsparse class is not needed, and will detract from what they will want to see.
- [fixed] (m.r) 1. ln 26: why create a function _assert_equals instead of using self.assertEquals defined in unittest.TestCase? https://docs.python.org/2/library/unittest.html#unittest.TestCase.assertEqual
GitLab
- [will not fix at present] (m.r/a.d) Could use Python3 for better arg parsing)
word_count_py/test/test_word_count_py.py
- [this happens automagically by argparse] (s.y) 23: print "Usage: ...." if wrong args
- [fixed] (m.r) . ln 37.  A class level field (self._parser) is added inside a method. I think it is better practise to declare all class level fields in the constructor rather than introduce them in methods. At least initialize to None in the constructor.  In this particular case rather than make _parser a class level field maybe just have it be a method local var. parser = argparse.ArgPar()... since it is not used anywhere else
- [argparse safeley deals with this] (m.r) ln 101: why are you only passing args with index 1 + onwards? why drop arg 0 here? I'm mostly concerned that argparse parser doesnt barf or skip args later on because it is expecting the args to contain that index 0, full command arg.
- [it is better to separate the printing from the the implementation] (m.r.) why is the printer a separate class instead of just another method in the WordCounter class?  curiosity question only
- [understood, I like temp vars] (s.y.) 60: temp_counter unnecessary
- [argparse catches this] (m.r)  Is a None input test required? Eg: file name is none, and/or file does not exist, file exists but does not allow read access
- [added] (m.r)  ln 38: would be good to add some help text, maybe even type checking. Eg: parser.add_argument('file_name',   help='cjones help text').  Specifically I'm interested in help text that specifies whether the file name input is an absolute path or relative path. I've run the word count app and looks like the code handles either option correctly so might be worth clarifying in the help text that both are acccepted. Also see comment in thread above Re: using python3 argparse to validate input file existence and permissions
- [not fixing at present] (s.y): not in the requirements, but there is no way to get _all_ results?  I'd change "get_top_results" to "get_results" with an optional parameter "n", where, if specified, returns only the top n results.  Internally, if n == 0 then it returns all results.
- [done] (s.y) 22: I tend to just put this code in line 101
- [sure later] (s.y): 43: this class has no instance variables, again I'd probably just put it in main
- [understood, but I was trying to function things] (s.y):  I'd just fold this into _sanitize_and_split_line
- [done] (s.y) I'd put comments explaining the params and output format
