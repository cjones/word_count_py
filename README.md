Problem description
==========
Write a program which accepts a filename as input and counts the frequency of all the words in the file.
The program must print the top ten most frequently occurring words in the file and the count for each word in descending order.
When counting words, punctuation must be ignored and words that differ in case only should be considered the same word:
e.g. "Why", "why" and "why?" should count as 3 occurrences of "why".
The file may be assumed to contain ASCII.

You should use a Test Driven Development (TDD) approach to developing your solution.

You may use the internet to help if you wish.
If the question is ambiguous, feel free to make assumptions and state these in your solution.
Please provide your working code, unit tests, test data and any instructions necessary for compiling and running your code.

How to use this program
==========

There are two ways to use this program:

1. pip install this package and import the module.
2. create an instance of the WordCounter class

-- or --

1. use the python command line: python word_count_py.py followed by  the name of the file to parse
Ex: > python word_count_py.py my_input_file.txt

How to test this program
==========

This program has two main tests and should work under most python frameworks as long as pip and tox have been installed.
The remaining packages are installed and run from within a virtual environment to prevent polution of the development system.

1. The standard PEP8 tests, which are tested via tox:

> tox -epep8

2. The standard unit tests.

> tox -epy27

Additional verbosity may assist in debugging by inserting the `-v` flag to tox.
