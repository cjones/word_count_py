# vim: tabstop=4 shiftwidth=4 softtabstop=4
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import unittest

from word_count_py import word_count_py


class TestWordCountPy(unittest.TestCase):

    @classmethod
    def setup_class(cls):
        cls._counter = word_count_py.WordCounter()

    def test_empty_file(self):
        file_name = './word_count_py/test/test_data_empty.txt'
        result = self._counter.get_results(file_name, 10)
        expected = None
        self.assertEqual(result, expected)

    def test_ipsum_lorem_generator(self):
        file_name = './word_count_py/test/test_data_1.txt'
        result = self._counter.get_results(file_name, 10)
        expected = [('in', 15),
                    ('vel', 10),
                    ('ante', 9),
                    ('id', 9),
                    ('a', 9),
                    ('et', 8),
                    ('vitae', 8),
                    ('lacus', 7),
                    ('eu', 7),
                    ('nec', 7)]
        self.assertEqual(result, expected)

    def test_punctuation_generator(self):
        file_name = './word_count_py/test/test_data_2.txt'
        result = self._counter.get_results(file_name, 10)
        expected = [('la', 32),
                    ('that', 9),
                    ('so', 9),
                    ('stop', 8),
                    ('pigeon', 8),
                    ('i', 8),
                    ('the', 8),
                    ('to', 7),
                    ('my', 7),
                    ('and', 5)]
        self.assertEqual(result, expected)
