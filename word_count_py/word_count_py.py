#!/usr/bin/python
# vim: tabstop=4 shiftwidth=4 softtabstop=4
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import argparse
import collections
import os
import string
import sys


class WordCounterArgParse(object):

    def __init__(self, args):
        self._file_name = self._parse_args_and_get_filename(args)

    def get_file_name(self):
        return self._file_name

    def _parse_args_and_get_filename(self, args):
        self.parser = argparse.ArgumentParser()
        help_text = 'relative or absolute path name'
        self.parser.add_argument('file_name', help=help_text)
        args = self.parser.parse_args()
        return args.file_name


class WordCounterPrinter(object):

    def print_items(self, items):
        if items:
            for item in items:
                print("Word: {}, Count: {}".format(item[0], item[1]))
        else:
            print("Empty File")


class WordCounter(object):

    def get_results(self, file_name, n):
        """Example function with PEP 484 type annotations.

        Args:
            file_name: The name of the file either in absolute or relative.
            n: The number of results to return

        Returns:
            The the top n results.
        """
        self._word_counts = collections.Counter()
        self._file_name = file_name
        self._parse_file()
        if self._word_counts:
            temp_counter = self._word_counts.most_common(n)
            return temp_counter

    def print_results(self, file_name, n):
        top_results = self.get_results(file_name, n)
        if top_results:
            wc_printer = WordCounterPrinter()
            wc_printer.print_items(top_results)

    def _parse_file(self):
        self._check_if_file_exists_or_exit()
        self._open_file_and_parse_data()

    def _check_if_file_exists_or_exit(self):
        exists = os.path.isfile(self._file_name)
        if not exists:
            self._print_and_exit()

    def _print_and_exit(self):
        print("File not found named: {}. ".format(self._file_name))
        sys.exit()

    def _open_file_and_parse_data(self):
        with open(self._file_name, 'r') as open_file:
            for line in open_file:
                split_line = self._sanitize_and_split_line(line)
                wc = self._word_counts
                self._word_counts = wc + collections.Counter(split_line)

    def _sanitize_and_split_line(self, line):
        sanitized_line = self._sanitize_line(line)
        split_line = sanitized_line.split()
        return split_line

    def _sanitize_line(self, line):
        line = line.lower()
        line = line.translate(None, string.punctuation)
        return line


def main(args):
    arg_parser = WordCounterArgParse(args)
    wc = WordCounter()
    wc.print_results(arg_parser.get_file_name(), 10)


if __name__ == "__main__":
    main(sys.argv[1:])
