from setuptools import setup

setup(
    name='word_count_py',
    version='0.1',
    install_requires=[],
    packages=['word_count_py'],
    package_data={'word_count_py': ['*.py']},
    entry_points='''
        [console_scripts]
        word_count_py=word_count_py.shell:cli
    ''',
    test_suite='nose.collector',
    tests_require=['nose'],
)
